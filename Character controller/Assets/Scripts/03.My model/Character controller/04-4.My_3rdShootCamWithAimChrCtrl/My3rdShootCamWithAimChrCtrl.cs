using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class My3rdShootCamWithAimChrCtrl : MonoBehaviour
{
    CharacterController ctrl;
    Animator anim;
    public GameObject followTarget;

    // 1. Move
    public float speed;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 verticalVelocity;

    // 3. IsGrounded
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    const float groundCheckRadius = 0.5f;       // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    // 4. Jump
    public float jumpHeight = 1.2f;             // 점프 높이
    public float JumpInterval = 1f;             // 점프 Interval
    float jumpTimeout;                          // 다음 점프까지 남은 시간

    // 5. Virtual cam
    public CinemachineVirtualCamera vcam;
    public CinemachineVirtualCamera vcamAim;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        GravityAndJump();
        GroundedCheck();
        Move();
    }

    void GravityAndJump()
    {
        if (isGrounded)
        {
            // isGrounded 가 된 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            if (verticalVelocity.y < 0)
            {
                verticalVelocity.y = -2f;
            }

            // 점프를 눌렀고, 점프가능하면 => 점프
            if (Input.GetKey(KeyCode.LeftControl) && jumpTimeout <= 0.0f)
            {
                // 위로 뛰므로, verticalVelocity가 양수가 된다.
                verticalVelocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);

                // Model
                anim.SetTrigger("Jump");
            }

            // 점프타임아웃을 시간만큼 줄인다.
            if (jumpTimeout >= 0.0f)
            {
                jumpTimeout -= Time.deltaTime;
            }
        }
        else
        {
            // 점프타임아웃을 리셋한다.
            jumpTimeout = JumpInterval;
        }

        verticalVelocity.y = verticalVelocity.y + (gravity * Time.deltaTime);
        ctrl.Move(verticalVelocity * Time.deltaTime);
    }

    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    void Move()
    {
        if (isGrounded && Input.GetKey(KeyCode.LeftAlt))
        {
            /******************************************************
             * 주준시 움직임
            ******************************************************/
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

            if (dir != Vector3.zero)
            {
                // World의 좌표축을 기준으로 움직인다.
                //ctrl.Move(dir * speed * Time.deltaTime); // Default: World space 사용

                // Local의 좌표축을 기준으로 움직인다. 
                dir = transform.TransformDirection(dir); // Local space를 World space로 변경 (Move 메소드에 맞게)
                ctrl.Move(dir * input.magnitude * Time.deltaTime);

                // Model
                anim.SetFloat("Speed_f", input.magnitude);
            }
            else
            {
                // Model
                anim.SetFloat("Speed_f", 0);
            }

            // Virtual camera
            vcam.gameObject.SetActive(false);
            vcamAim.gameObject.SetActive(true);

            // Target move: 조준점을 이동시킨다.
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            Vector3 verticalRot = new Vector3(-mouseY, 0, 0);
            if (verticalRot != Vector3.zero)
            {
                followTarget.transform.Rotate(verticalRot);

                Vector3 clampedRot = followTarget.transform.rotation.eulerAngles;

                //Clamp the Up/Down rotation
                if (clampedRot.x > 180 && clampedRot.x < 330)
                {
                    clampedRot.x = 330;// 조준할수있는 가장 높은 각도
                }
                else if (clampedRot.x < 180 && clampedRot.x > 40)
                {
                    clampedRot.x = 40;// 조준할 수 있는 가장 낮은 각도
                }

                followTarget.transform.rotation = Quaternion.Euler(clampedRot);
            }

            Vector3 horizontalRot = new Vector3(0, mouseX, 0);
            if (horizontalRot != Vector3.zero)
                transform.Rotate(horizontalRot);
        }
        else
        {
            /******************************************************
             * 보통 움직임
            ******************************************************/
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            // 뒤로 버튼이 눌린 경우, 스피드를 마이너스로
            if (input.y < 0)
                speed = -1;
            else
                speed = Mathf.Abs(input.x) + Mathf.Abs(input.y);

            // 방향 계산시, 뒤돌아보지 않도록 하기 위해, z는 항상 양수로
            input.y = Mathf.Abs(input.y);

            Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

            if (dir != Vector3.zero)
            {
                float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

                // 회전을 천천히 하게 만듬
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, targetAngle, 0f), 2 * Time.deltaTime);

                Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

                ctrl.Move(moveDir * speed * Time.deltaTime); // Default: World space 사용

                // Model
                anim.SetFloat("Speed_f", input.magnitude);

                // Virtual cam: 달리는 중에 Noise를 추가한다.
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1;
            }
            else
            {
                // Model
                anim.SetFloat("Speed_f", 0);

                // Virtual cam: 멈추면 Noise를 멈춘다.
                vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
            }

            // Virtual camera
            vcam.gameObject.SetActive(true);
            vcamAim.gameObject.SetActive(false);

            // Target move
            followTarget.transform.localRotation = Quaternion.Euler(Vector3.zero); // 조준점을 원래대로
        }
    }
}