using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyRotate : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;

    public float rotSpeed = 1;
    Vector3 rot;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rot = new Vector3(input.y * rotSpeed, input.x * rotSpeed, 0);

        //transform.Rotate(rot);                  // ���� ��ǥ�� ����. Default: Space.Self
        //transform.Rotate(rot, Space.World);   // World ��ǥ�� ����

        //rb.AddTorque(rot * rotSpeed); // World ��ǥ�� ����. Default: ForceMode.Force
        rb.AddRelativeTorque(rot * rotSpeed); // ���� ��ǥ�� ����. Default: ForceMode.Force
    }
}