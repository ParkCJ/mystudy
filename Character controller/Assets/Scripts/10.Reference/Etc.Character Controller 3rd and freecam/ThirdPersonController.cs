using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 1. 스크립트 외 설정
///     - Virtual cam에 Extension으로 Cinemachine collider 설정
///         Collide against: 가리는 벽으로 인식할 Layer
///         Ignore tag: 무시할 GameObject
///         Strategy: 가려질 경우 어떻게 할 것인지
///             Pull camera forward: Player와 벽 사이로 카메라를 이동 (보통 게임에 많이 쓰는 방식)
/// 2. 참고
///     https://www.youtube.com/watch?v=4HpC--2iowE
/// </summary>
public class ThirdPersonController : MonoBehaviour
{
    public CharacterController player;

    // 1. Player 이동
    public float speed = 6f;

    // 3. Camera가 바라보는 방향이 정면이 되도록
    public Transform cam;

    // Update is called once per frame
    void Update()
    {
        // 1. Player 이동
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 dir = new Vector3(x, 0f, z).normalized;

        if (dir.magnitude > 0.1f)
        {
            // 2. Player의 정면이, 눌린 방향키 방향을 바라보도록
            //float targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            // 3. 2 + Camera가 바라보는 방향이 정면이 되도록
            float targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            player.Move(moveDir.normalized * speed * Time.deltaTime);
        }
    }
}
