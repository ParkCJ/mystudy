using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

// 여기서부터 다시하기
public class MyFreeBackCam : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;

    // 1. Move
    public float moveForce = 1000;
    public float maxSpeed = 2;
    float speed;

    // 2. IsGrounded
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    const float groundCheckRadius = 0.5f;       // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    // 3. Jump
    public float jumpForce = 1f;                // 점프 Force
    public float JumpInterval = 1f;             // 점프 Interval
    float jumpTimeout;                          // 다음 점프까지 남은 시간

    // 4. Virtual cam
    public CinemachineFreeLook fcam;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        Jump();
        GroundedCheck();
        Move();
        Config();
    }

    void Jump()
    {
        if (isGrounded)
        {
            // 점프를 눌렀고, 점프가능하면 => 점프
            if (Input.GetKey(KeyCode.LeftControl) && jumpTimeout <= 0.0f)
            {
                // 위로 뛰므로, verticalVelocity가 양수가 된다.
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

                // Model
                anim.SetTrigger("Jump");
            }

            // 점프타임아웃을 시간만큼 줄인다.
            if (jumpTimeout >= 0.0f)
            {
                jumpTimeout -= Time.deltaTime;
            }
        }
        else
        {
            // 점프타임아웃을 리셋한다.
            jumpTimeout = JumpInterval;
        }
    }

    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    void Move()
    {
        if (isGrounded)
        {
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            // 뒤로 버튼이 눌린 경우, 스피드를 마이너스로
            if (input.y < 0)
                speed = -1;
            else
                speed = Mathf.Abs(input.x) + Mathf.Abs(input.y);

            // 방향 계산시, 뒤돌아보지 않도록 하기 위해, z는 항상 양수로
            input.y = Mathf.Abs(input.y);

            Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

            if (dir != Vector3.zero)
            {
                float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

                transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

                Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

                // 로컬 좌표축을 기준으로 움직인다.
                dir = transform.TransformDirection(dir);
                if (rb.velocity.magnitude < maxSpeed)
                    rb.AddForce(moveDir * moveForce * speed * Time.deltaTime); // Default: ForceMode.Force
                else
                    rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.

                // Model
                anim.SetFloat("Speed_f", input.magnitude);
            }
            else
            {
                // Model
                anim.SetFloat("Speed_f", 0);
            }
        }
    }

    void Config()
    {
    }
}