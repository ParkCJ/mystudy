using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 0. 이동을 만들때 고려할 사항
/// 1) Gravity
/// 2) Jumping
/// 3) Drag
/// 4) In-air movement
/// 5) Slopes
/// 6) Steps
/// 7) Sprinting
/// 8) Crouching
/// 9) Ohysics objects
/// 
/// 1. Unity에서 이동은 2가지 방법 중 선택해 구현한다.
/// 1) Character controller 컴포넌트 사용
///     - Handle steps
///     - Handle slopes
///     - Doesn't get stuck on walls
///     - Easy to make snappy
///     
/// 2) RigitBody 컴포넌트 사용
///     - Gravity built in
///     - Drag built in
///     - Interacts with physics objects
///     
/// 2. 그 외
/// GroundCheck Sphear 만들기:
///     - Radius는 Character controller의 Radius와 동일 => 예) 0.5
///     - Y위치: 구의 1/4 부분이 바닥에 닫게. 계산법: 캐릭터의 가장 하단 높이 - (Radius/2) => 예) 0 - (0.5/2) = -0.25
/// 점프공식: v = 루트(높이 * -2 * 중력)
/// 
/// 3. 참고
/// https://www.youtube.com/watch?v=4HpC--2iowE
/// </summary>
public class PlayerMovement3rd : MonoBehaviour
{
    public CharacterController controller;

    // 1. Move
    public float speed = 7f;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 velocity;
    bool isGrounded;

    // 3. IsGrounded
    public Transform groundCheck; // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask; // Ground로 사용되는 GameObject들의 Layer를 Ground로 설정한다.

    // 4. Jump
    public float jumpHeight = 1.2f;

    // Update is called once per frame
    void Update()
    {
        // 3. IsGrounded
        // Physics.CheckSphear: 반지름 크기의 구를 만들어, 그 구가 groundMask와 충돌상태인지 검사한다.
        isGrounded = Physics.CheckSphere(groundCheck.position, 0.5f, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            // 위에서 isGrounded를 체크한 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            // velocity.y = 0f;
            velocity.y = -2f;
        }

        // 1. Move
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 dir = new Vector3(x, 0f, z).normalized;

        if (dir.magnitude > 0.1f)
        {
            // 2. Player의 정면이, 눌린 방향키 방향을 바라보도록
            float targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            controller.Move(dir * speed * Time.deltaTime);
        }

        // 4. Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        // 2. Gravity
        velocity.y = velocity.y + (gravity * Time.deltaTime); // 가속을 구현하기 위해 velocity.y를 더해준다.
        controller.Move(velocity * Time.deltaTime);
    }
}
