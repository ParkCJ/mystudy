using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCMove : MonoBehaviour
{
    CharacterController ctrl;

    public float speed = 2f;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            // World의 좌표축을 기준으로 움직인다.
            //ctrl.Move(dir * speed * Time.deltaTime); // Default: World space 사용

            // Local의 좌표축을 기준으로 움직인다. 
            dir = transform.TransformDirection(dir); // Local space를 World space로 변경 (Move 메소드에 맞게)
            ctrl.Move(dir * speed * Time.deltaTime);
        }
    }
}