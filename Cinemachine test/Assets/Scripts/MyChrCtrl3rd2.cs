using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyChrCtrl3rd2 : MonoBehaviour
{
    CharacterController chrCtrl;

    // 1. 이동
    [Header("Move")]
    public float walkSpeed = 2f;                // 걷는 속도
    public float sprintSpeed = 4f;              // 뛰는 속도
    public float speedChangeRate = 10.0f;       // 속도가 변화율
    float targetSpeed;
    float currSpeed;

    // 2. 회전
    [Header("Rotation")]
    public float rotationSmoothTime = 0.12f;    // 목표 각도까지 회전하는데 걸리는 시간
    float targetRotation;
    float rotationVelocity;                     // 현재의 회전 가속도

    // 3. 중력
    [Header("Gravity")]
    public float gravity = -15f;                //-9.81f;
    float verticalVelocity;                     // Player의 현재 Y포지션

    // 4. IsGrounded
    [Header("IsGrounded")]
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    float groundCheckRadius = 0.5f;             // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    // 5. Jump
    [Header("Jump")]
    public float jumpHeight = 1.2f;             // 점프 높이
    public float JumpInterval = 1f;             // 점프 Interval
    float jumpTimeout;                          // 다음 점프까지 남은 시간


    void Awake()
    {
        chrCtrl = GetComponent<CharacterController>();
    }

    void Start()
    {
        jumpTimeout = JumpInterval;
    }

    // Update is called once per frame
    void Update()
    {
        GravityAndJump();
        GroundedCheck();
        Move();
    }

    /// <summary>
    /// Player의 Y 포지션값을 계산 (중력과 Jump를 감안)
    /// </summary>
    void GravityAndJump()
    {
        if (isGrounded)
        {
            // isGrounded 가 된 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            if (verticalVelocity < 0)
            {
                verticalVelocity = -2f;
            }

            // 점프를 눌렀고, 점프가능하면 => 점프
            if (Input.GetButtonDown("Jump") && jumpTimeout <= 0.0f)
            {
                verticalVelocity = Mathf.Sqrt(jumpHeight * -2 * gravity);
            }

            // 점프타임아웃을 시간만큼 줄인다.
            if (jumpTimeout >= 0.0f)
            {
                jumpTimeout -= Time.deltaTime;
            }
        }
        else
        {
            // 점프타임아웃을 리셋한다.
            jumpTimeout = JumpInterval;
        }

        verticalVelocity += (gravity * Time.deltaTime);
    }

    /// <summary>
    /// IsGrounded를 계산
    /// </summary>
    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    /// <summary>
    /// Input을 사용해 Player를 회전
    /// Input, Player의 Y포지션을 사용해 Player를 이동
    /// </summary>
    void Move()
    {
        // 1. input 값을 받아
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (input == Vector2.zero)
            targetSpeed = 0.0f;
        else
        {
            // 2. 속도 계산
            targetSpeed = Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : walkSpeed; // 걷는지, 뛰는지
            currSpeed = new Vector3(chrCtrl.velocity.x, 0.0f, chrCtrl.velocity.z).magnitude;
            float speedOffset = 0.1f;
            float inputMagnitude = 1f;//_input.analogMovement ? _input.move.magnitude : 1f;

            if (currSpeed < targetSpeed - speedOffset || currSpeed > targetSpeed + speedOffset)
            {
                targetSpeed = Mathf.Lerp(currSpeed, targetSpeed * inputMagnitude, Time.deltaTime * speedChangeRate); // 속도 보정. 가속, 감속 하도록
                targetSpeed = Mathf.Round(targetSpeed * 1000f) / 1000f;
            }

            // 3. 회전 각도 계산
            Vector3 dir = new Vector3(input.x, 0f, input.y).normalized;
            targetRotation = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;// + Camera.main.transform.eulerAngles.y;
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref rotationVelocity, rotationSmoothTime); // 각도 보정. 가속하도록

            // 4. Player를 회전
            transform.rotation = Quaternion.Euler(0f, rotation, 0f);
        }

        // 5. 방향 계산
        Vector3 targetDir = (Quaternion.Euler(0.0f, targetRotation, 0.0f) * Vector3.forward).normalized;

        // 6. Player를 이동 (Y축 이동이 있기 때문에, input이 없어도 실행해야 한다.)
        chrCtrl.Move(targetDir * (targetSpeed * Time.deltaTime) + new Vector3(0.0f, verticalVelocity, 0.0f) * Time.deltaTime);
    }
}