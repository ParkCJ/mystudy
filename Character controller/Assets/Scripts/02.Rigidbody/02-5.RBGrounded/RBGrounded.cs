using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBGrounded : MonoBehaviour
{
    Rigidbody rb;

    // 2. IsGrounded
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    const float groundCheckRadius = 0.5f;       // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        GroundedCheck();
        Move();
    }

    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    void Move()
    {
        if (isGrounded)
        {
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

            if (dir != Vector3.zero)
            {
                float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg; // World 기준
                                                                       //float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; // 카메라가 보는 방향 기준

                transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

                Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

                rb.velocity = moveDir * 5; // World 기준
            }
        }
    }
}