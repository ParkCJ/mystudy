using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 3. 참고
/// https://www.youtube.com/watch?v=_QajrabyTJc
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    // 1. Move
    public float speed = 2f;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 velocity;
    bool isGrounded;

    // 3. IsGrounded
    public Transform groundCheck; // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask; // Ground로 사용되는 GameObject들의 Layer를 Ground로 설정한다.

    // 4. Jump
    public float jumpHeight = 1.2f;

    // Update is called once per frame
    void Update()
    {
        // 3. IsGrounded
        // Physics.CheckSphear: 반지름 크기의 구를 만들어, 그 구가 groundMask와 충돌상태인지 검사한다.
        isGrounded = Physics.CheckSphere(groundCheck.position, 0.5f, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            // 위에서 isGrounded를 체크한 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            // velocity.y = 0f;
            velocity.y = -2f;
        }

        // 1. Move
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 dir = (transform.right * x) + (transform.forward * z);

        controller.Move(dir * speed * Time.deltaTime);

        // 4. Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        // 2. Gravity
        velocity.y = velocity.y + (gravity * Time.deltaTime);
        controller.Move(velocity * Time.deltaTime);
    }
}
