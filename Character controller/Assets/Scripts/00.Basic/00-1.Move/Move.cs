using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed = 2f;

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            transform.Translate(dir * speed * Time.deltaTime);                  // 로컬 좌표축을 기준으로 움직인다. Default: Space.Self
            //transform.Translate(dir * speed * Time.deltaTime, Space.World);   // World의 좌표축을 기준으로 움직인다.
        }
    }
}