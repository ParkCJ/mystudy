using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCFace : MonoBehaviour
{
    CharacterController ctrl;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        // 중요! 아무것도 입력을 안하면 => Vector3.zero가 되어 => 0도가 되어 => World 기준점 (0, 0, 0) + 0도가 되어 => World 기준점 (0, 0, 0)을 향하게 된다.
        // 안돌아가게 하려면, 입력이 없는 경우는 처리하지 않도록 해야함
        // 
        // [Local 기준 계산]
        // Local 기준 계산을 하려면, Local 기준점을 만들어줘야함
        // Local 기준점이 자신의 Rotation이 되면, Update가 한번 실행될때마다 값이 변하므로, 뺑글뺑글 돌게됨
        // 따라서 Local 기준점은 변하지 않는 걸로 해야 함 (카메라의 시점 등)
        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg; // World 기준
            //float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; // Local 기준점을 카메라의 Y회전값으로 잡는다. 카메라의 Y회전값을 변경하며 테스트 가능

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
        }
    }
}