using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionBlurScene : MonoBehaviour
{
    // MotionBlur 효과: URP에서는 카메라 움직임에만 적용된다. (카메라 회전 등). 물체의 움직임에는 사용 불가능
    // 상시적용시키면 부하가 있으니, 특정 순간에는 동작하도록 (대쉬 등. Motion blur가 들어가있는 Global volume를 추가하고, 대쉬 순간에만 Weight를 1로 변경)
    public float rotSpeed = 300;

    void LateUpdate()
    {
        float angle = Mathf.PingPong(Time.time * rotSpeed, 20) - 10; // between 350 ~ 10
        Camera.main.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
}
