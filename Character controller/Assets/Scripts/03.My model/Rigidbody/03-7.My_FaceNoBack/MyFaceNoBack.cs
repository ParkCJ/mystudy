using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 바이오하자드 레벌레이션 무브
/// 앞, 뒤버튼: 이동
/// 옆 버튼: 돌기 (현재 카메라 정면이 계산에 포함되므로 돌기가 된다.)
/// </summary>
public class MyFaceNoBack : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;

    public float moveForce = 1000;
    public float maxSpeed = 2;
    float speed;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        // 뒤로 버튼이 눌린 경우, 스피드를 마이너스로
        if (input.y < 0)
            speed = -1;
        else
            speed = Mathf.Abs(input.x) + Mathf.Abs(input.y);

        // 방향 계산시, 뒤돌아보지 않도록 하기 위해, z는 항상 양수로
        input.y = Mathf.Abs(input.y);

        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

            // 회전을 천천히 하게 만듬
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, targetAngle, 0f), 2 * Time.deltaTime);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            // 로컬 좌표축을 기준으로 움직인다.
            dir = transform.TransformDirection(dir);
            if (rb.velocity.magnitude < maxSpeed)
                rb.AddForce(moveDir * moveForce * speed * Time.deltaTime); // Default: ForceMode.Force
            else
                rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.

            // Model
            anim.SetFloat("Speed_f", input.magnitude);
        }
        else
        {
            // Model
            anim.SetFloat("Speed_f", 0);
        }
    }
}