using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Aiming : MonoBehaviour
{
    public CinemachineVirtualCamera vCam1;
    public CinemachineVirtualCamera vCamAiming;
    public GameObject reticle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            vCam1.gameObject.SetActive(false);
            vCamAiming.gameObject.SetActive(true);
        }
        else
        {
            vCam1.gameObject.SetActive(true);
            vCamAiming.gameObject.SetActive(false);
        }
    }
}
