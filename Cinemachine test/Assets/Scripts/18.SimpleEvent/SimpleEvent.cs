using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Cinemachine.Examples;

public class SimpleEvent : MonoBehaviour
{
    public CinemachineVirtualCamera cam1;
    public CinemachineBlendListCamera cam2;
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(true);

            player.GetComponent<CharacterMovement>().enabled = false;
            player.GetComponent<Animator>().enabled = false;
        }
    }
}
