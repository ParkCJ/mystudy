using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBRotate : MonoBehaviour
{
    Rigidbody rb;

    public float rotSpeed = 1000;
    Vector3 rot;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rot = new Vector3(input.y * rotSpeed, input.x * rotSpeed, 0);

        //transform.Rotate(rot);                  // ���� ��ǥ�� ����. Default: Space.Self
        //transform.Rotate(rot, Space.World);   // World ��ǥ�� ����

        //rb.AddTorque(rot * rotSpeed); // World ��ǥ�� ����. Default: ForceMode.Force
        rb.AddRelativeTorque(rot * rotSpeed); // ���� ��ǥ�� ����. Default: ForceMode.Force
    }
}