using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceThenMoveFreecamManual : MonoBehaviour
{
    float currRotY = 0;

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + currRotY; // World 기준

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            transform.Translate(moveDir * 5 * Time.deltaTime, Space.World);
        }

        // Z 키를 누르면, 캐릭터가 현재 시점을 바라보게 한다.
        if (Input.GetKey(KeyCode.Z))
        {
            transform.rotation = Quaternion.Euler(0f, Camera.main.transform.eulerAngles.y, 0f);
            currRotY = Camera.main.transform.eulerAngles.y;
        }
    }
}