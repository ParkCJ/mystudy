using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magazine : MonoBehaviour
{
    public Transform bulletPositions;
    public GameObject bullets;
    public GameObject bulletPrefab;

    [Header("Config")]
    public float bulletMoveSpeed = 7;
    public float comboInitHeight = 10;

    [Header("Debug")]
    public GameObject bulletToFire;
    public int comboCnt;
    int bulletIdx;

    // Start is called before the first frame update
    void Start()
    {
        // Init bullets
        for (int i = 0; i < bulletPositions.childCount; i++)
        {
            GameObject b = Instantiate(bulletPrefab, bulletPositions.GetChild(i).position, Quaternion.identity, bullets.transform);
            b.name = bulletIdx.ToString();
            bulletIdx++;
        }
        bulletToFire = bullets.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        MoveBullet();
        FireBullet();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        bulletToFire = col.gameObject;

        comboCnt++;
    }

    void MoveBullet()
    {
        int bIdx = 0;
        foreach (Transform bullet in bullets.transform)
        {
            Transform target = bulletPositions.GetChild(bIdx);

            bullet.position = Vector3.Lerp(bullet.position, target.position, bulletMoveSpeed * Time.deltaTime);
            if (Vector3.Distance(bullet.position, target.position) < 1f)
                bullet.position = target.position;

            bullet.localScale = Vector3.Lerp(bullet.localScale, target.localScale, bulletMoveSpeed * Time.deltaTime);
            if (Vector3.Distance(bullet.localScale, target.localScale) < 0.01f)
                bullet.localScale = target.localScale;
            /*
            bullet.position = Vector3.MoveTowards(bullet.position, bulletPosList[idx].position, bulletMoveSpeed * Time.deltaTime);//250
            if (Vector3.Distance(bullet.position, bulletPosList[idx].position) < 1f)
                bullet.position = bulletPosList[idx].position;

            bullet.localScale = Vector3.Lerp(bullet.localScale, bulletPosList[idx].localScale, bulletScaleUpSpeed * Time.deltaTime);//5
            if (Vector3.Distance(bullet.localScale, bulletPosList[idx].localScale) < 0.01f)
                bullet.localScale = bulletPosList[idx].localScale;
            
            */

            /*Vector3 newPos = Vector3.MoveTowards(bullet.position, bulletPosList[idx].position, bulletMoveSpeed);
            if (!Mathf.Approximately(Vector3.Distance(bullet.position, bulletPosList[idx].position), 0))
            {
                bullet.position = newPos;
                bullet.localScale = Vector3.Lerp(bullet.localScale, bulletPosList[idx].localScale, bulletMoveSpeed * Time.deltaTime);
            }*/

            bIdx++;
        }

        if (Vector3.Distance(bullets.transform.GetChild(0).position, bulletPositions.GetChild(0).position) < comboInitHeight)
            comboCnt = 0;
    }

    void FireBullet()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (bulletToFire != null)
            {
                Debug.Log("Fire");

                // 1. Fire
                // FireAction

                // 2. Combo
                comboCnt++;

                // 3. Destroy
                Destroy(bulletToFire);

                // 4. Instantiate new bullet
                GameObject b = Instantiate(bulletPrefab, bulletPositions.GetChild(bulletPositions.childCount - 1).position, Quaternion.identity, bullets.transform);
                b.name = bulletIdx.ToString();
                bulletIdx++;
            }
        }
    }
}
