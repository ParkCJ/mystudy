// 항상 같은 속도로 움직이게 할 때
// Ground 에서만 적용하도록 한다. 안그러면 중력 가속도도 적용안됨
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMove : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;

    public float moveForce = 1000;
    public float maxSpeed = 2;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            Debug.Log($"dir.mag:{input.magnitude}");
            // World의 좌표축을 기준으로 움직인다.
            //rb.velocity = dir * maxSpeed; // World 기준

            // 로컬 좌표축을 기준으로 움직인다.
            dir = transform.TransformDirection(dir);
            if (rb.velocity.magnitude < maxSpeed)
                rb.AddForce(dir * moveForce * Time.deltaTime); // Default: ForceMode.Force
            else
                rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.

            // Model
            anim.SetFloat("Speed_f", input.magnitude);
        }
        else
        {
            // Model
            anim.SetFloat("Speed_f", 0);
        }
    }
}