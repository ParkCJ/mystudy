using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCGravity : MonoBehaviour
{
    CharacterController ctrl;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 gravityVelocity;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        Gravity();
        Move();
    }

    void Gravity()
    {
        gravityVelocity.y = gravityVelocity.y + (gravity * Time.deltaTime);
        ctrl.Move(gravityVelocity * Time.deltaTime);
    }

    void Move()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg; // World 기준
            //float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; // 카메라가 보는 방향 기준

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            ctrl.Move(moveDir * 5 * Time.deltaTime); // Default: World space 사용
        }
    }
}