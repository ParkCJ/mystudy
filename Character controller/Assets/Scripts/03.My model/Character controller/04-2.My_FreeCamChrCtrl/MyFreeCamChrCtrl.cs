using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MyFreeCamChrCtrl : MonoBehaviour
{
    CharacterController ctrl;
    Animator anim;

    // 1. Move
    public float speed = 5;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 verticalVelocity;

    // 3. IsGrounded
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    const float groundCheckRadius = 0.5f;       // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    // 4. Jump
    public float jumpHeight = 1.2f;             // 점프 높이
    public float JumpInterval = 1f;             // 점프 Interval
    float jumpTimeout;                          // 다음 점프까지 남은 시간

    // 4. Virtual cam
    public CinemachineFreeLook fcam;
    bool isStopping; // 멈춰있다가 달리기 시작할 때, fcam의 높이를 달리기가 잘 보이는 위치로 고정

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        GravityAndJump();
        GroundedCheck();
        Move();
    }

    void GravityAndJump()
    {
        if (isGrounded)
        {
            // isGrounded 가 된 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            if (verticalVelocity.y < 0)
            {
                verticalVelocity.y = -2f;
            }

            // 점프를 눌렀고, 점프가능하면 => 점프
            if (Input.GetKey(KeyCode.LeftControl) && jumpTimeout <= 0.0f)
            {
                // 위로 뛰므로, verticalVelocity가 양수가 된다.
                verticalVelocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);

                // Model
                anim.SetTrigger("Jump");
            }

            // 점프타임아웃을 시간만큼 줄인다.
            if (jumpTimeout >= 0.0f)
            {
                jumpTimeout -= Time.deltaTime;
            }
        }
        else
        {
            // 점프타임아웃을 리셋한다.
            jumpTimeout = JumpInterval;
        }

        verticalVelocity.y = verticalVelocity.y + (gravity * Time.deltaTime);
        ctrl.Move(verticalVelocity * Time.deltaTime);
    }

    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    void Move()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            ctrl.Move(moveDir * speed * Time.deltaTime); // Default: World space 사용

            // Model
            anim.SetFloat("Speed_f", input.magnitude);

            // Free cam: 멈춰있다가 달리기 시작할 때 잘 보이도록, 달리기 시작하면 달리기 높이로 변경한다.
            if (isStopping)
            {
                fcam.m_YAxis.Value = 0.75f;
                isStopping = false;
            }
        }
        else
        {
            // Model
            anim.SetFloat("Speed_f", 0);

            // Flag
            isStopping = true;
        }
    }
}