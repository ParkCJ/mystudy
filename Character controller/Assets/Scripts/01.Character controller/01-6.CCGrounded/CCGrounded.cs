using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCGrounded : MonoBehaviour
{
    CharacterController ctrl;

    // 2. Gravity
    public float gravity = -15f;//-9.81f;
    Vector3 gravityVelocity;

    // 3. IsGrounded
    public Transform groundCheck;               // Player의 바닥 높이에 붙인다.
    public LayerMask groundMask;                // Ground Layer를 설정
    const float groundCheckRadius = 0.5f;       // Character controller의 Radius와 동일하게 설정
    bool isGrounded;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        Gravity();
        GroundedCheck();
        Move();
    }

    void Gravity()
    {
        if (isGrounded)
        {
            // isGrounded 가 된 순간, 완전히 지면에 닿아있지 않을 수 있으므로, 지면에 완전히 닿도록, 중력을 약간 준다.
            if (gravityVelocity.y < 0)
            {
                gravityVelocity.y = -2f;
            }
        }

        gravityVelocity.y = gravityVelocity.y + (gravity * Time.deltaTime);
        ctrl.Move(gravityVelocity * Time.deltaTime);
    }

    void GroundedCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);
    }

    void Move()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg; // World 기준
            //float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; // 카메라가 보는 방향 기준

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            ctrl.Move(moveDir * 5 * Time.deltaTime); // Default: World space 사용
        }
    }
}