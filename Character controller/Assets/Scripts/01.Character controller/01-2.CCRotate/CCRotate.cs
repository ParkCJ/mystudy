using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCRotate : MonoBehaviour
{
    CharacterController ctrl;

    public float rotSpeed = 5;
    Vector3 rot;

    void Awake()
    {
        ctrl = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rot = new Vector3(input.y * rotSpeed, input.x * rotSpeed, 0);

        transform.Rotate(rot);                  // ���� ��ǥ�� ����. Default: Space.Self
        //transform.Rotate(rot, Space.World);   // World ��ǥ�� ����
    }
}