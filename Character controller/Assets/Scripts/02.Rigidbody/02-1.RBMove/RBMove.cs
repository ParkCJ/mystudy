// 가속하면서 움직이게 할 때
// Max speed를 넘어가면, 역방향으로 Force를 가해 넘어가지 못하게 한다.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMove : MonoBehaviour
{
    Rigidbody rb;

    public float moveForce = 1000;
    public float maxSpeed = 2;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        // World 기준
        //if (rb.velocity.magnitude < maxSpeed)
        //    rb.AddForce(dir * moveForce * Time.deltaTime); // Default: ForceMode.Force
        //else
        //    rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.

        // Local 기준
        dir = transform.TransformDirection(dir);
        if (rb.velocity.magnitude < maxSpeed)
            rb.AddForce(dir * moveForce * Time.deltaTime); // Default: ForceMode.Force
        else
            rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.
    }
}

/*
// 항상 같은 속도로 움직이게 할 때
// Ground 에서만 적용하도록 한다. 안그러면 중력 가속도도 적용안됨
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMove : MonoBehaviour
{
    Rigidbody rb;

    public float maxSpeed = 2;
    float x;
    float z;
    Vector3 dir;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
        dir = new Vector3(x, 0f, z).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            // World의 좌표축을 기준으로 움직인다.
            //rb.velocity = dir * maxSpeed; // World 기준

            // 로컬 좌표축을 기준으로 움직인다.
            dir = transform.TransformDirection(dir);
            rb.velocity = dir * maxSpeed;
        }
    }
}
*/
