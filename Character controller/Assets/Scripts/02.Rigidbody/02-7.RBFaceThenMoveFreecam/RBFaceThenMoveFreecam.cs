using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBFaceThenMoveFreecam : MonoBehaviour
{
    Rigidbody rb;

    public float moveForce = 1000;
    public float maxSpeed = 2;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준

            // World 기준
            if (rb.velocity.magnitude < maxSpeed)
                rb.AddForce(moveDir * moveForce * Time.deltaTime); // Default: ForceMode.Force
            else
                rb.AddForce(rb.velocity.normalized * (maxSpeed - rb.velocity.magnitude));  // 초과속도 만큼 움직이는 역방향으로 Force를 가한다.
        }
    }
}