using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class LensDistortionScene : MonoBehaviour
{
    /*
    * <Lens Distortion> 화면 뒤틀림
    * - Intensity: 0=보통, 음수=오목렌즈, 양수=볼록렌즈
    * - X, Y multiplyer: 축별 뒤틀림 정도 곱하기(0: 뒤틀림 없음)
    * - Center X, Y: 렌즈의 중심
    * - Scale: 줌인 정도. 1=보통, 1이상: 줌인, 1이하: 타일형태
    */

    public Button concaveBtn;
    public Button normalBtn;
    public Button convexBtn;

    Volume ppVol;
    LensDistortion lensDistortion;

    void Awake()
    {
        ppVol = GameObject.Find("Global Volume").GetComponent<Volume>();
        ppVol.profile.TryGet(out lensDistortion);

        concaveBtn.onClick.AddListener(() =>
        {
            lensDistortion.intensity.value = -1f;
            lensDistortion.scale.value = 0.5f;
        });

        normalBtn.onClick.AddListener(() =>
        {
            lensDistortion.intensity.value = 0f;
            lensDistortion.scale.value = 1;
        });

        convexBtn.onClick.AddListener(() =>
        {
            lensDistortion.intensity.value = 1f;
            lensDistortion.scale.value = 1;
        });
    }
}
