using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 마우스입력을 받아, Player를 회전시킨다.
// Main camera를 Player의 자식으로 두기 때문에, Player가 회전하면 Main cam도 같이 회전한다.
// 위, 아래 회전 (X rotation)은 Clamp로 상하단 90도로 제한한다. (제한하지 않으면 뒤로 넘어간다.)
public class MouseLook : MonoBehaviour
{
    public float mouseSensitive = 100f;

    public Transform player;

    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitive * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitive * Time.deltaTime;

        xRotation = xRotation - mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        player.Rotate(Vector3.up * mouseX);
    }
}
