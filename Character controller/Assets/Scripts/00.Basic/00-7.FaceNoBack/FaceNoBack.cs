using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 바이오하자드 레벌레이션 무브
/// 앞, 뒤버튼: 이동
/// 옆 버튼: 돌기 (현재 카메라 정면이 계산에 포함되므로 돌기가 된다.)
/// </summary>
public class FaceNoBack : MonoBehaviour
{
    float speed;

    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        // 뒤로 버튼이 눌린 경우, 스피드를 마이너스로
        if (input.y < 0)
            speed = -1;
        else
            speed = Mathf.Abs(input.x) + Mathf.Abs(input.y);

        // 방향 계산시, 뒤돌아보지 않도록 하기 위해, z는 항상 양수로
        input.y = Mathf.Abs(input.y);

        Vector3 dir = new Vector3(input.x, 0f, input.y).normalized; // Local space 기준

        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

            // 회전을 천천히 하게 만듬
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, targetAngle, 0f), 2 * Time.deltaTime);

            Vector3 moveDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized; // World space 기준
            transform.Translate(moveDir * speed * Time.deltaTime, Space.World);
        }
    }
}

/*
 * using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceNoBack : MonoBehaviour
{
    float speed;
    float x;
    float z;

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        speed = Mathf.Abs(x) + z;

        if (z < 0)
        {
            z = Mathf.Abs(z);
        }

        Vector3 dir = new Vector3(x, 0f, z).normalized;

        // 중요! 아무것도 입력을 안하면 => Vector3.zero가 되어 => 0도가 되어 => World 기준점 (0, 0, 0) + 0도가 되어 => World 기준점 (0, 0, 0)을 향하게 된다.
        // 안돌아가게 하려면, 입력이 없는 경우는 처리하지 않도록 해야함
        // 
        // [Local 기준 계산]
        // Local 기준 계산을 하려면, Local 기준점을 만들어줘야함
        // Local 기준점이 자신의 Rotation이 되면, Update가 한번 실행될때마다 값이 변하므로, 뺑글뺑글 돌게됨
        // 따라서 Local 기준점은 변하지 않는 걸로 해야 함 (카메라의 시점 등)
        if (dir != Vector3.zero)
        {
            float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg; // World 기준
            //float targetAngle = Mathf.Atan2(x, z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y; // Local 기준점을 카메라의 Y회전값으로 잡는다. 카메라의 Y회전값을 변경하며 테스트 가능

            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

            Vector3 newDir = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized;
            transform.Translate(newDir * -2 * Time.deltaTime, Space.World);
        }
    }
}
*/
